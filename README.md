# Coding Challenges



## Welcome!

This project repository holds my progress in working through various coding challenges from Neetcode, Codewars, and HackerRank. While I have worked on questions before, now I am planning to keep a record. As I continue to build my technical skills, I hope to document my thought process and record concepts I am solidifying through my solve. I hope to not only provide viable solutions to the challenges, but learn how to communicate well about the logic behind my work.

/*
A recruiter has a list of applicants and the list of available jobs, and needs to determine which jobs each applicant has relevant skills for (at least 1 skill must match for an applicant to be considered).

Please write a Javascript function, jobMatcher(), that takes in the applicants array and the jobListings array and outputs an array of objects - each object representing one applicant and the jobs they're eligible for (just the strings of the job titles) like below:

let applicantEligibility = [
 {
  applicantName: Lindsey,
  jobsEligibleFor: ["Doctor", "Teacher", "Lawyer"]
 },
 {
  applicantName: Drew,
  jobsEligibleFor: ["Dentist", "Dental Hygienist"]
 }
]
and the jobListings data looks like this:

let jobListings = [
  {
     jobTitle: "Front Desk Manager",
     jobLocation: "Minneapolis",
     workRemoteAvailable: false,
     skillsRequired: ["Typing", "Organization"],
     salary: 68000
  },
  {
     jobTitle: "Healthcare Consultant",
     jobLocation: "Detroit",
     workRemoteAvailable: true,
     skillsRequired: ["Healthcare Knowledge", "Punctuality"],
     salary: 91000
  },
]
and the applicantList looks like:

const applicantList = [
  {
    name: "Andrew",
    age: 33,
    location: "New York",
    skills: ["graphic design", "Javascript"],
    currentSalary: 77000
   },
  {
    name: "Maggie",
    age: 31,
    location: "Denver",
    skills: ["retail management","material processing"],
    currentSalary: 55000
   },
  {
    name: "Zane",
    age: 34,
    location: "Lewistown",
    skills: ["legal", "clerical"],
    currentSalary: 80000
   }
]
*/

// Solution:
function jobMatcher(applicants, jobs) {
    let applicantEligibility = []

    applicants.forEach(applicant => {
        let qualifiedJobs = []

        jobs.forEach(job => {
            let hasSkill = job.skillsRequired.some(skill => applicant.skills.includes(skill))
            if (hasSkill) {
                qualifiedJobs.push(job.jobTitle)
            }
        })

        applicantEligibility.push({
            applicantName: applicant.name,
            jobsEligibleFor: qualifiedJobs
        })
    })

    return applicantEligibility
}

// Notes

/*
First attempt, I used multiple for loops. However, found a better solution using the forEach and .some
built in JavaScript functions to go through array items instead.
*/

# https://leetcode.com/problems/reverse-linked-list/description/

# Solution:
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        prev = None
        current = head

        while current is not None:
            next_node = current.next
            current.next = prev
            prev = current
            current = next_node
        return prev

"""
Notes:

By using variables to keep track of the next value and the previous value,
we're able to switch the ordering in the linked list.
"""

# Hack Reactor Knowledge Check

# Prompt:
# The following function accepts a list of integers, depths.
# Complete the function to return the number of times the depth increases for sum in a three-measurement sliding window.

# My Solution:

def count_depth_increases(depths):
    prev_num = 0
    curr_num = 0
    inc_count = 0

    for i in range(0, (len(depths) - 2)):
        curr_num = sum(depths[i:(i + 3)])
        if prev_num == 0:
            prev_num = curr_num
            continue
        if curr_num > prev_num:
            inc_count += 1
        prev_num = curr_num

    return inc_count

"""
Notes:

By iterating through the indices of depths, I was able to use the sum Python built in function
to return the sum of the current number + the next two numbers. If prev_num is 0, that means
this is the first measurement and has nothing to compare itself with, so it needs to continue
to the next iteration of the loop after setting the prev_num to the current sum. At first,
I considered that the worst case time complexity of my solution is O(n^2), because of the for loop and sum function.
However, technically it would be O(n), because the sum function is not iterating through all of depths,
but just the immediate 3 values. This is excluding the other mathematical operations that would
also contribute to the time complexity. This is linear time.
"""

# Given Solution:

# The simplest way to do this is to just loop over the list from the first index to the third-to-last index, calculate the sum of those three numbers, and compare the current sum with the next sum on each iteration.

def count_depth_increases(depths):
    count = 0
    for i in range(len(depths) - 3):
        first_sum = depths[i] + depths[i + 1] + depths[i + 2]
        next_sum = depths[i + 1] + depths[i + 2] + depths[i + 3]
        if first_sum < next_sum:
            count += 1
    return count

# This is \( O(n) \), so that's not bad.

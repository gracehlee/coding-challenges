# https://neetcode.io/problems/is-palindrome

"""
Is Palindrome
Given a string s, return true if it is a palindrome, otherwise return false.

A palindrome is a string that reads the same forward and backward. It is also case-insensitive and ignores all non-alphanumeric characters.

Example 1:

Input: s = "Was it a car or a cat I saw?"

Output: true
Explanation: After considering only alphanumerical characters we have "wasitacaroracatisaw", which is a palindrome.

Example 2:

Input: s = "tab a cat"

Output: false
Explanation: "tabacat" is not a palindrome.

Constraints:

1 <= s.length <= 1000
s is made up of only printable ASCII characters.
"""

# Solution

class Solution:
    def isPalindrome(self, s: str) -> bool:
        letters = [char.lower() for char in s if char.isalnum()]

        length = len(letters)
        halfLength = 0

        if length <= 1:
            return True

        if length % 2 == 0:
            halfLength = length // 2
        else:
            halfLength = (length - 1) // 2

        half1 = letters[:halfLength]
        half2 = letters[-1:-(halfLength+1):-1]

        if half1 == half2:
            return True
        else:
            return False

"""
Notes:

Took a few iterations because a single character string and an empty string
is considered a palindrome (originally I had those return false).
"""

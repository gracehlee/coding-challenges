# https://leetcode.com/problems/group-anagrams/description/

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        d = {}

        for string in strs:
            letters = tuple(sorted(string))
            if letters not in d.keys():
                d[letters] = [string]
            else:
                d[letters].append(string)

        result = list(d.values())
        return result

"""
Notes: Learned that I can't use lists as dictionary keys,
so had to use tuples instead. Learned to use the tuple() function
similar to how list() works.

Time complexity: sorted() takes O(k log k) time, where k is the
number of letters for each string. Creating tuple takes O(k) time.
This brings our total worst case time complexity to O(N * k log k),
where N is the length of strs.
"""

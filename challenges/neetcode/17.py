# https://neetcode.io/problems/two-integer-sum-ii

"""
Two Integer Sum II
Given an array of integers numbers that is sorted in non-decreasing order.

Return the indices (1-indexed) of two numbers, [index1, index2], such that they add up to a given target number target and index1 < index2. Note that index1 and index2 cannot be equal, therefore you may not use the same element twice.

There will always be exactly one valid solution.

Your solution must use
𝑂
(
1
)
O(1) additional space.

Example 1:

Input: numbers = [1,2,3,4], target = 3

Output: [1,2]
Explanation:
The sum of 1 and 2 is 3. Since we are assuming a 1-indexed array, index1 = 1, index2 = 2. We return [1, 2].

Constraints:

2 <= numbers.length <= 1000
-1000 <= numbers[i] <= 1000
-1000 <= target <= 1000

"""

# Solution

class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        oneList = list(enumerate(numbers, start=1))

        for i in range(len(oneList)):
            for j in range(i + 1, len(oneList)):
                if (oneList[i][1] + oneList[j][1]) == target:
                    return [oneList[i][0], oneList[j][0]]

        return []

"""
Notes:

By using enumerate function, I was able to have the given list 1-indexed.
Then using counters i and j to keep track of the indices, I was able to
compare i and j values until target value was found, or return an empty list.
"""

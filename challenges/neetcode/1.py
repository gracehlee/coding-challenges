# https://leetcode.com/problems/contains-duplicate/description/

"Attempt 1"

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        d = {}
        for num in nums:
            if d.get(num) == None:
                d[num] = 0
            d[num] += 1
        for val in d.values():
            if val == 2:
                return True
        return False

# this solution passes only 69 / 75 test cases.

"Attempt 2"

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        d = {}
        for num in nums:
            if d.get(num) == None:
                d[num] = 1
                continue
            d[num] += 1
        for val in d.values():
            if val > 1:
                return True
        return False

"""
I realized that my previous solution didn't account for
duplicates found more than 2 times, so I adjusted my if statement.
Time complexity is O(n) linear time.
"""

# Another given solution:

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        unique = set(nums)
        if(len(nums)>len(unique)):
            return True
        else:
            return False
"""
I like this solution because it has a very simple logic-
no need to test every number for a duplicate, but rather,
check if a duplicate ever existed by checking list length.
This is also O(n) linear time.
"""

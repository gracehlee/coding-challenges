# https://neetcode.io/problems/trapping-rain-water

"""
Trapping Rain Water
You are given an array non-negative integers heights which represent an elevation map. Each value heights[i] represents the height of a bar, which has a width of 1.

Return the maximum area of water that can be trapped between the bars.

Example 1:
Input: height = [0,2,0,3,1,0,1,3,2,1]

Output: 9
Constraints:

1 <= height.length <= 1000
0 <= height[i] <= 1000

"""

# Current Attempt

class Solution:
    def trap(self, heights: List[int]) -> int:
        maximum = 0

        current = 0
        water = 0

        for height in heights:
            if current == 0:
                current = height
            if current != 0 and height < current and height != 0:
                water = water + current - height
            if current != 0 and height == 0:
                water += current
            if height >= current:
                maximum += water
                current = height
                water = 0

        return maximum

"""
Notes:

This solution only worked if the next bar of height we were checking
was greater or equal to the current bar height; it did not work if the
second bar height was less than the first bar height. While this worked
for some test cases, it did not pass all.
"""

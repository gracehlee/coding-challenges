# https://neetcode.io/problems/max-water-container

"""
Max Water Container
You are given an integer array heights where heights[i] represents the height of the 𝑖𝑡ℎ ith bar.

You may choose any two bars to form a container. Return the maximum amount of water a container can store.

Example 1:
Input: height = [1,7,2,5,4,7,3,6]

Output: 36

Example 2:

Input: height = [2,2,2]

Output: 4
Constraints:

2 <= height.length <= 1000
0 <= height[i] <= 1000

"""

# Solution

class Solution:
    def maxArea(self, heights: List[int]) -> int:
        length = len(heights)

        maximum = 0

        for i in range(length):
            for j in range(i + 1, length):
                width = j - i
                height = 0
                if heights[j] > heights[i]:
                    height = heights[i]
                else:
                    height = heights[j]
                area = width * height
                if area > maximum:
                    maximum = area

        return maximum

"""
Notes:

If the two heights (or poles) of the container are not the same,
then the water can be filled only up to the lesser pole height.
By finding the lowest value of the poles, then taking the difference
between the indices to find the width, we were able to calculate the
area of the container and update the maximum volume value.
"""

# https://neetcode.io/problems/products-of-array-discluding-self

class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        result = []
        product = 1
        left = []
        right = []

        for num in nums:
            left.append(product)
            product *= num

        product = 1

        for i in range(len(nums)-1, -1, -1):
            right.insert(0, product)
            product *= nums[i]

        for i in range(len(nums)):
            result.append(left[i] * right[i])

        return result

"""
Notes:

This was the first solution I wrote for this problem.
Its time complexity is quite poor, as it is O(n^2) quadratic time.
This is due to the use of the insert method, which takes O(n) time
to run through each iteration of the for loop.
"""

class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        n = len(nums)
        result = [1] * n

        left_product = 1
        for i in range(n):
            result[i] = left_product
            left_product *= nums[i]

        right_product = 1
        for i in range(n - 1, -1, -1):
            result[i] *= right_product
            right_product *= nums[i]

        return result

"""
Attempt 2:

This is a better solution because by dividing the products into
two separate for loops, we avoid the need to use the insert method.
Time complexity is O(n) which is linear time.
"""

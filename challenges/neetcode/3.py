# https://leetcode.com/problems/two-sum/description/

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        result = []

        for i in range(0, len(nums)):
            for j in range(i + 1, len(nums)):
                if (nums[i] + nums[j]) == target and len(result) < 2:
                    result.append(i)
                    result.append(j)

        return result

"""
I had to take a few tries to figure out the correct range for the index.
At first, I used range(1, len(nums)) for the j counter, only to realize
that caused duplicates by potentially considering the same pair twice.

Time complexity is O(n^2) due to the nested for loops, which is quadratic time.
A better solution is to use a dictionary; here is a provided solution:
"""

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        pair_idx = {}

        for i, num in enumerate(nums):
            if target - num in pair_idx:
                return [i, pair_idx[target - num]]
            pair_idx[num] = i

# this has a time complexity of O(n), which is linear time.

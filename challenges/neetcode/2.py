# https://leetcode.com/problems/valid-anagram/description/

"Attempt 1"

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        l = [char for char in t]

        for i in range(0, len(s)):
            if s[i] in l and t[i] in s:
                l.remove(s[i])
            else:
                return False

        if len(l) == 0:
            return True
        return False

"""
At first, I tried using the .split method to create a list of the characters in t,
but that didn't work because it would split it into words. So instead, I opted to use
list comprehension. I also incorrectly assumed that if s and t were identical,
they were not anagrams. After removing that incorrect conditional, my code worked.
Time complexity is O(n^2) which is quadratic time.

Time complexity could be improved by using dictionaries instead. I will try again another time.
"""

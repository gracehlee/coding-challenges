# https://leetcode.com/problems/top-k-frequent-elements/

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        d= {}
        for num in nums:
            if d.get(num) == None:
                d[num] = 0
            d[num] += 1

        counts = sorted(d.items(), key=lambda item: item[1], reverse=True)

        result = []
        for i in range(0, k):
            result.append(int(counts[i][0]))

        return result

"""
Notes: I had to look into the sorted python function, specifically to apply
reverse=True to list the dictionary items in descending order, so that the
highest occuring items will be listed first. Dictionary keys are also stored
as strings, so I had to convert them into integers before appending to result.

Time complexity: O(N log N)
"""

# https://neetcode.io/problems/buy-and-sell-crypto

"""
Buy and Sell Crypto
You are given an integer array prices where prices[i] is the price of NeetCoin on the ith day.

You may choose a single day to buy one NeetCoin and choose a different day in the future to sell it.

Return the maximum profit you can achieve. You may choose to not make any transactions, in which case the profit would be 0.

Example 1:

Input: prices = [10,1,5,6,7,1]

Output: 6
Explanation: Buy prices[1] and sell prices[4], profit = 7 - 1 = 6.

Example 2:

Input: prices = [10,8,7,5,2]

Output: 0
Explanation: No profitable transactions can be made, thus the max profit is 0.

Constraints:

1 <= prices.length <= 100
0 <= prices[i] <= 100

"""

# Attempt 1

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        least = 0
        most = 0
        profit = 0

        for i in range(len(prices)):
            if least == 0:
                least = prices[i]
            if prices[i] < least:
                least = prices[i]
            if prices[i] != least:
                if most == 0:
                    most = prices[i]
                most = prices[i]
            money = most - least
            if money > profit:
                profit = money

        return profit

"""
This did not keep track of whether indices were after the current indices.
"""

# Solution

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        profit = 0

        for i in range(len(prices)):
            least = prices[i]
            for j in range(i + 1, len(prices)):
                most = prices[j]

                if most - least > profit:
                    profit = most - least


        return profit

"""
I realized I didn't need to keep track of least and most outside of the
for loops, since using the iterators i and j will compare all the rest of
the prices to the current i value anyways. Profit is only updated IF
there was a greater profit value found.
"""

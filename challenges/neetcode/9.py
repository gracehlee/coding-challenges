# https://leetcode.com/problems/longest-consecutive-sequence/

class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        if not nums:
            return 0

        sorted_nums = sorted(set(nums))
        longest = 1
        current = 1

        for i in range(1, len(sorted_nums)):
            if sorted_nums[i] == sorted_nums[i - 1] + 1:
                current += 1
                longest = max(longest, current)
            else:
                current = 1

        return longest

"""
Notes:
This solution runs in O(n) linear time. At first, I used only the sorted
function, but that didn't account for duplicates in nums. By using the
set function as well, I'm able to eliminate duplicates and count the
length of the longest consecutive chain of numbers.
"""

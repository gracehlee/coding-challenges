# https://leetcode.com/problems/valid-sudoku/description/

class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        # easiest: check all the rows
        #       get all the numerical values
        #       any repeats, return false
        for row in board:
            d = {}

            if !((1 in row) and (2 in row) and (3 in row) and (4 in row) and (5 in row) and (6 in row and 7 in row and 8 in row and 9 in row)):
                return False

        # next: check all the columns
        #       for all the lists, index[i]
        #       if any repeats, return false
        for i in range(0, 9):
            if i not in [board[0][i], board[1][i], board[2][i], board[3][i], board[4][i], board[5][i], board[6][i], board[7][i], board[8][i]]:
                return False

        # hardest: check the squares
        #       generate lists of each 9 square grid box
        #       if any repeats, return false
        square1 = [board[0][0], board[1][1]]

"""
Notes:

This was my first incomplete (failed) attempt at this problem.
There's increasing complexity with each of the 3 goals.
This particular solution would not work correctly as the board
may only have some of the numbers defined, not all, for each
of the checks.
"""

class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        # check rows
        for row in board:
            lst = []
            for i in range(0, 9):
                if row[i] in lst:
                    return False
                if row[i] != '.':
                    lst.append(row[i])
        # check columns
        for i in range(0, 9):
            lst = []
            for j in range(0, 9):
                if board[j][i] in lst:
                    return False
                if board[j][i] != '.':
                    lst.append(board[j][i])
        # check squares
        square = []
        square2 = []
        square3 = []
        for i in range(0, 9):
            if i == 3 or i == 6:
                square = []
                square2 = []
                square3 = []
            for j in range(0, 3):
                if board[j][i] in square:
                    return False
                if board[j][i] != '.':
                    square.append(board[j][i])
            for k in range(3, 6):
                if board[k][i] in square2:
                    return False
                if board[k][i] != '.':
                    square2.append(board[k][i])
            for l in range(6, 9):
                if board[l][i] in square3:
                    return False
                if board[l][i] != '.':
                    square3.append(board[l][i])
        return True

"""
Notes:
My second attempt yielded a working solution. There are 3 phases of checks:
1. Check the rows
2. Check the columns
3. Check the squares
Numbers are only appended to the list for a check if they are not a '.',
and if at any point a check fails, the function returns False.

Since there is a set number of rows and columns and squares in the board,
time complexity is O(l) or constant time.
"""

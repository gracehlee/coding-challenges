# https://neetcode.io/problems/evaluate-reverse-polish-notation

"""
You are given an array of strings tokens that represents a valid arithmetic expression in Reverse Polish Notation.

Return the integer that represents the evaluation of the expression.

The operands may be integers or the results of other operations.
The operators include '+', '-', '*', and '/'.
Assume that division between integers always truncates toward zero.
Example 1:

Input: tokens = ["1","2","+","3","*","4","-"]

Output: 5

Explanation: ((1 + 2) * 3) - 4 = 5
Constraints:

1 <= tokens.length <= 1000.
tokens[i] is "+", "-", "*", or "/", or a string representing an integer in the range [-100, 100].
"""

# Attempt 1

"""
import operator

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        result = int(tokens[0])

        operands = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.truediv
        }

        for i in range(1, len(tokens)):
            if tokens[i] not in operands:
                result = operands[(tokens[i + 1])](result, int(tokens[i]))

        return result

Solution failed because the input doesn't include the operands and numbers in order of operation.
ie. input: tokens=["4","13","5","/","+"]
Needs to be translated to (4 / 13) + 5
"""

# Solution

import operator

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:

        operands = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": lambda x, y: int(x / y)
        }

        nums = []

        for token in tokens:
            if token in operands:
                # then we take the first two items in nums to apply operand to
                b = nums.pop()
                a = nums.pop()
                nums.append(operands[token](a,b))
            else:
                # if number, append to nums
                nums.append(int(token))

        return nums[0]

"""
Notes:
Had to look up how to convert string operands into actual operands.
Notation: operands_dictionary[string_operand](num1, num2) -> num1 <operand> num2

Also, originally had errors in my solution because of the decimal values.
Had to look up how to do integer division instead of operator.truediv which provides a float value.
Will review this question in future to hopefully solve without outside help.

"""

# https://leetcode.com/problems/min-stack/

# not sure I understand this problem, so I'm watching the video and taking notes.

"""
Stack supports adding value, popping value, etc.
- done through array or linked list.
By default, stack doesn't support int getMin() function.

Example:

Stack:
-3
 0
-2
How do we get the minimum?
Easy to think of a way to achieve it with O(n), but we need O(1) constant time.
If we pop a value that was the minimum, then the minimum needs to update.

Stack 2:
-3
-2
-2

By using a second stack to track the minimum with each position of the stack,
we can pop a value from both stacks, then get the most recent top of the stack 2
to get the true minimum of the stack.
"""

# Given Solution

class Minstack:
    def __init__(self):
        self.stack = []
        self.minStack = []

    def push(self, val:int) -> None:
        self.stack.append(val)
        # if stack is empty, then get min of val.
        # if stack is not empty, then get min from minStack.
        val = min(val, self.minStack[-1] if self.minStack else val)

    def pop(self) -> None:
        self.stack.pop()
        self.minStack.pop()

    def top(self) -> int:
        # last, or most recent value, of stack
        return self.stack[-1]

    def getMin(self) -> int:
        # last, or most recent value, of stack
        return self.minStack[-1]

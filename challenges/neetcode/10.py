# https://leetcode.com/problems/valid-parentheses/description/

class Solution:
    def isValid(self, s: str) -> bool:
        if s[0] in [')', ']', '}']:
            return False
        for i in range(0, len(s)):
            if s[i] == '(':
                if s[i + 1] != ')':
                    return False
            if s[i] == '{':
                if s[i + 1] != '}':
                    return False
            if s[i] == '[':
                if s[i + 1] != ']':
                    return False
        return True

"""
Attempt 1: did not succeed.
Notes:
According to Prompt 4, a valid string would include:
s = "{[]}"

I had to adjust my solution based on this information.
"""

class Solution:
    def isValid(self, s: str) -> bool:
        if s[0] in [')', ']', '}']:
            return False
        if len(s) < 2:
            return False
        pairs = {
            '(': ')',
            '[': ']',
            '{': '}',
        }
        open = pairs.keys()
        closed = pairs.values()
        for i in range(0, len(s) - 1):
            if s[i] in open and s[i + 1] in closed:
                if s[i] == '(':
                    if s[i + 1] != ')':
                        return False
                if s[i] == '{':
                    if s[i + 1] != '}':
                        return False
                if s[i] == '[':
                    if s[i + 1] != ']':
                        return False
            if s[i] in open and s[i + 1] in open:
                if s[i + 1] == s[len(s) - 1]:
                    return False
            if s[i] in closed:
                match = False
                index = 1
                while not match:
                    if i - index < 0:
                        return False
                    if s[i - index] in open and pairs[s[i - index]] != s[i]:
                        return False
                    if s[i - index] in closed:
                        index += 2
                    if s[i - index] in open and pairs[s[i - index]] == s[i]:
                        match = True
        return True

"""
Attempt 2: Only passes 83 / 98 test cases.
It failed: s = "(){}}{"

This solution is partially working, but it's not accounting for all edge cases.
I think there has to be a simpler solution than writing down each edge case.
I will start over and find a different solution.

"""
class Solution:
    def isValid(self, s: str) -> bool:
        if len(s) % 2 != 0:
            return False

        pairs = {
            '(': ')',
            '[': ']',
            '{': '}',
        }
        open_brackets = set(pairs.keys())
        closed_brackets = set(pairs.values())

        stack = []
        for bracket in s:
            if bracket in open_brackets:
                stack.append(bracket)
            elif bracket in closed_brackets:
                if not stack or pairs[stack.pop()] != bracket:
                    return False

        return len(stack) == 0

"""
Attempt 3:

This solution works, with time complexity O(n) linear time.
If the string length is odd, then we know there will be a mismatched pair,
so we can rule out this edge case. By using a stack list variable,
we're able to keep track of open brackets to compare with closing brackets.
"""

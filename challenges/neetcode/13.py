# https://neetcode.io/problems/generate-parentheses

"""
Generate Parentheses

You are given an integer n. Return all well-formed parentheses strings that you can generate with n pairs of parentheses.

Example 1:

Input: n = 1

Output: ["()"]
Example 2:

Input: n = 3

Output: ["((()))","(()())","(())()","()(())","()()()"]
You may return the answer in any order.

Constraints:

1 <= n <= 7

"""

# thoughts

"""
class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        result = []

        start = "("
        a = n - 1

        ending = ")"
        b = n - 1

        for i in range (0, n):
            string = ""

Result will be an array of strings.
I was thinking that the first and last characters of each string
must always start with an open parenthesis and end with a closing parenthesis.

But I'm not sure how to set the various combinations at the center,
because some combinations will not be proper parentheses ie. ())(()
Some possible approaches- decide the first half, then generate second half;
or start at center and work your way out from it. But a unique center pair
can still have more than one possible valid string combination.
ie. () -> ()()() OR ((()))

"""

# Video solution notes

"""
n = 1 -> ["()"]

Video solution did have a similar approach in saying that first and last
characters of the strings will be ( and ). Also similarly mentioned that
there is a limit # of each kind of parenthesis.

We can only add an closing parenthesis IF # close so far < # open.

Conditions:
n open, n close
close < open

n = 3

[ ( ]  : close < open , so we can add EITHER ( or )
[((] : close still < open, so we can add EITHER ( or )
but for [()]: close = open, so we can only add (

and when there is n number of (, then the only option left is ).

"""

# Given solution

class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        stack = []
        result = []

        # recursive function
        def backtrack(openN, closedN):
            # add to result when one stack is complete
            if openN == closedN == n:
                result.append("".join(stack))
                return

            if openN < n:
                stack.append("(")
                backtrack(openN + 1, closedN)
                stack.pop()

            if closedN < openN:
                stack.append(")")
                backtrack(openN, closedN + 1)
                stack.pop()

        backtrack(0, 0)
        return result

"""
Recursive function with 3 conditions to track.
"""
